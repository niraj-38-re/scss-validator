module.exports =  {
    'srcDir' : './scss_dir/incorrect_format',
    'srcFile' : 'NotRootCssClass.scss',
    'ignoreFiles' : [ 'NoMatch.css'],
    'spaceTagClassNameCheck' : ['body', 'div', 'span', 'a'],
    'containTagClassNameCheck' : ['body', 'div' , 'span']

}
