<a name="run"></a>

## run(configObject)
A exported main function.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| configObject |  | Information about the configFile. |
| configObject.srcDir | <code>string</code> | The source directory for sccs. default value "." |
| configObject.srcFile | <code>string</code> | The single scss file. if provided only single file will be scaned. |
| configObject.ignoreFiles | <code>Array.&lt;string&gt;</code> | The ignoreFiles. default value [] |
| configObject.spaceTagClassNameCheck | <code>Array.&lt;string&gt;</code> | Array of html tag ClassName to chek in scss. default value ['body', 'div', 'span','a'] |
| configObject.containTagClassNameCheck | <code>Array.&lt;string&gt;</code> | Array of contain html tag ClassName to chek in scss. default value [] |

