module.exports =  {
    'srcDir' : './scss_dir/correct_format',
    'srcFile' : '',
    'ignoreFiles' : [ 'NoMatch.css'],
    'spaceTagClassNameCheck' : ['body', 'div', 'span', 'a'],
    'containTagClassNameCheck' : ['body', 'div' , 'span']

}
