const fs = require('fs');
const path = require('path');
const readline = require('readline');
const glob = require("glob");
/**
  * A exported main function.
  * @param configObject Information about the configFile.
  * @param {string} configObject.srcDir The source directory for sccs. default value "."
  * @param {string} configObject.srcFile The single scss file. if provided only single file will be scaned.
  * @param {string[]} configObject.ignoreFiles The ignoreFiles. default value []
  * @param {string[]} configObject.spaceTagClassNameCheck Array of html tag ClassName to chek in scss. default value ['body', 'div', 'span','a']
  * @param {string[]} configObject.containTagClassNameCheck Array of contain html tag ClassName to chek in scss. default value []
  */

function run(configObject) {
    console.log('provided configObject : \n', configObject);


    // console.log(path.join(__dirname,  configFileName))
    // let configFile = configFileName ? require(configFileName) : {
    //     'srcDir' : '.',
    //     'ignoreFiles' : [ ],
    //     'spaceTagClassNameCheck' : ['body', 'div', 'span','a'],
    //     'containTagClassNameCheck' : []
    // };
    let baseConfig = {
        'srcDir' : '.',
        'ignoreFiles' : [ ],
        'spaceTagClassNameCheck' : ['body', 'div', 'span','a'],
        'containTagClassNameCheck' : []
    }
    let configFile = Object.assign(baseConfig, configObject);
    console.log('\nconfigObject : \n', configFile);

    // add all time css files
    let ignoreFiles = ['**/*.css'];
    // check data type of configFile.ignoreFiles
    // console.log(configFile.ignoreFiles.constructor  , typeof configFile.ignoreFiles)
    if (configFile.ignoreFiles) {

        ignoreFiles = ignoreFiles.concat(configFile.ignoreFiles)
    }
    const srcDir = path.join(path.resolve("."), configFile.srcDir);
    // console.log('ignoreFiles : ', ignoreFiles);
    glob("**/*.scss", { cwd: srcDir, ignore: ignoreFiles }, function (er, filesArrayFromSrcDir) {
        let filesArray = [];
        if(configFile.srcFile){
            filesArray = [ configFile.srcFile ];
        }else{
            filesArray=  filesArrayFromSrcDir;
        }

        for (var i = 0; i < filesArray.length; i++) {
            const fileName = srcDir + "/" + filesArray[i];

            const directories = path.dirname(fileName);
            // console.log(directories);
            const basename = path.basename(fileName);
            // console.log(basename)
            let errorFlag = false;
            console.log(fileName);
            let tempFileNameArray = fileName.split("/");
            let expectedClassName = tempFileNameArray[tempFileNameArray.length - 1].split(".")[0];
            // const scssFile = directories + '/' + expectedClassName + '.scss';

            const rl = readline.createInterface({
                input: fs.createReadStream(fileName),
                crlfDelay: Infinity
            });
            const filecontextArray = []
            rl.on('line', (line) => {
                // console.log(`Line from file: ${line}`);
                if (line.indexOf('//') > -1 || line.indexOf('@import') > -1) {
                    // do nothing
                } else {
                    filecontextArray.push(line)
                }
            }).on('close', () => {

                // console.log(filecontextArray);
                const filecontext = filecontextArray.join("");
                // console.log(filecontext);

                const regex2 = /\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$/gm

                // let found2 = filecontext.match(regex2);

                const data = filecontext.replace(regex2, '');
                // console.log(data);

                var counter = 1;
                // var firstIndex = data.indexOf('{');
                // console.log("firstIndex for '{' at ",firstIndex);
                var tempIndex = data.indexOf('.'+expectedClassName+'-component');
                // console.log(tempIndex);
                var tempData = data.substr(tempIndex);
                // console.log(data);
                // console.log(tempData);
                var secondtempIndex =  tempData.indexOf('{');
                var firstIndex = tempIndex + secondtempIndex;
                console.log("firstIndex for " + expectedClassName + "-component {' at ", firstIndex);

                var flag = false;
                for (var i = firstIndex + 1; i < data.length; i++) {

                    // console.log(i,data[i]);
                    if (data[i] === '{') {
                        counter++
                    } else if (data[i] === '}') {
                        counter--
                    } else {
                        // do nothing
                    }


                    if (counter === 0) {
                        // console.log(i,data[i],counter);
                        // console.log('once counter is < 1 i.e first time zero i.e content eval is finished ')
                        flag = true;

                    }

                    // dead code
                    if (flag && counter > 0) {
                        // more opening braces "{" than closing braces "}"  or MultipleRootClass
                        break;
                    }else if(flag && counter < 0){
                        // more closing braces "}" than opening braces "{"  or  expectedClassName + "-component is not root css class
                        break;
                    }else{
                        //
                    }
                }

                if (flag && counter > 0) {
                    console.log('wrong scss format');
                    errorFlag = true;
                    throw new Error("Error on file " + fileName + " and error message: " + " wrong scss format. \n  MultipleRootClass ");

                } else if (flag && counter < 0) {
                   //
                   console.log('wrong scss format');

                   errorFlag = true;

                   throw new Error("Error on file " + fileName + " and error message: " + " wrong scss format. \n  ClassName " + expectedClassName + "-component is not root css class ");

                }else{
                    // console.log('right scss format');
                    // console.log("sccs file content ");
                    // console.log(data);

                    console.log("check for  '*' css selector in scss file");

                    const regex = '\\s\{1\}\\*\{1\}' + '\\s*{';
                    const bodyregex = new RegExp(regex, 'gm');
                    let bodymatch;
                    // console.log(bodyregex);
                    while ((bodymatch = bodyregex.exec(data)) !== null) {
                        // This is necessary to avoid infinite loops with zero-width matches
                        if (bodymatch.index === bodyregex.lastIndex) {
                            bodyregex.lastIndex++;
                        }

                        // The result can be accessed through the `match`-variable.
                        bodymatch.forEach((match, groupIndex) => {
                            console.log(`Found match, group ${groupIndex}: ${match}`);
                            errorFlag = true;
                            throw new Error("Error on file " + fileName + " and error message:  * " + " tag is used in css class ");

                        });

                    }

                    console.log('checking for other rules from config file');

                    if (configFile && configFile.spaceTagClassNameCheck && typeof Array.isArray(configFile.spaceTagClassNameCheck)) {
                        console.log('checking for spaceTagClassNameCheck', configFile.spaceTagClassNameCheck)
                        for (let i = 0; i < configFile.spaceTagClassNameCheck.length; i++) {

                            const regex = '\\s\{1\}' + configFile.spaceTagClassNameCheck[i] + '\\s*{';
                            const bodyregex = new RegExp(regex, 'gm');
                            let bodymatch;
                            console.log('regex for  spaceTagClassNameCheck for' + configFile.spaceTagClassNameCheck[i] + ' is ' + bodyregex);
                            while ((bodymatch = bodyregex.exec(data)) !== null) {
                                // This is necessary to avoid infinite loops with zero-width matches
                                if (bodymatch.index === bodyregex.lastIndex) {
                                    bodyregex.lastIndex++;
                                }

                                // The result can be accessed through the `m`-variable.
                                bodymatch.forEach((match, groupIndex) => {
                                    console.log(`Found match, group ${groupIndex}: ${match}`);
                                    errorFlag = true;
                                    throw new Error("Error on file " + fileName + " and error message: " + configFile.spaceTagClassNameCheck[i] + " tag is used in css class ");


                                });
                            }

                        }

                    }

                    if (configFile && configFile.containTagClassNameCheck && typeof Array.isArray(configFile.containTagClassNameCheck)) {
                        console.log('checking for containTagClassNameCheck', configFile.containTagClassNameCheck)
                        for (let i = 0; i < configFile.containTagClassNameCheck.length; i++) {

                            const regex = configFile.containTagClassNameCheck[i] + '\\s*{';
                            const bodyregex = new RegExp(regex, 'gm');
                            let bodymatch;
                            console.log('regex for containTagClassNameCheck for ' + configFile.spaceTagClassNameCheck[i] + ' is ' + bodyregex);
                            while ((bodymatch = bodyregex.exec(data)) !== null) {
                                // This is necessary to avoid infinite loops with zero-width matches
                                if (bodymatch.index === bodyregex.lastIndex) {
                                    bodyregex.lastIndex++;
                                }

                                // The result can be accessed through the `m`-variable.
                                bodymatch.forEach((match, groupIndex) => {
                                    console.log(`Found match, group ${groupIndex}: ${match}`);
                                    errorFlag = true;
                                    throw new Error("Error on file " + fileName + " and error message: " + configFile.containTagClassNameCheck[i] + " tag is used in css class ");


                                });
                            }

                        }

                    }





                    var classNameKeyReg = new RegExp('.' + expectedClassName + '-component', "g");
                    // var classNameKeyReg = new RegExp('.' + "abc" + '-component', "g");
                    // console.log(fileName , data.match(classNameKeyReg));
                    if (data.match(classNameKeyReg) === null || data.match(classNameKeyReg).length !== 1) {
                        console.log('component className "' + expectedClassName + '-component" is not used or used multiple time  ' + fileName);
                        errorFlag = true;
                        throw new Error('Error on file ' + fileName + ' and error message:  component className "' + expectedClassName + '-component" is not used or used multiple time');


                    } else {
                        console.log('Hurray it is working fine for ' + fileName);
                    }

                }

            });









        }
    })
}


// update npm version by CI/CD pipeline 2
module.exports = run;

// fix broken pipeline from broken ci/cd pipeline
