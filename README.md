# scss-validator
This library  will help to mandate rules for writing css in large enterprise UI component development. 


## Installation

This is a [Node.js](https://nodejs.org/) module available through the 
[npm registry](https://www.npmjs.com/). It can be installed using the 
[`npm`](https://docs.npmjs.com/getting-started/installing-npm-packages-locally)
or 
[`yarn`](https://yarnpkg.com/en/)
command line tools.

```sh
npm install scss-validator --save
```

## Usage 

```javascript

const scssObject = require('scss-validator');

const configObject = require('./SuccessScssConfig');

scssObject(configObject);


```
SuccessScssConfig.js - sample file 
```javascript

module.exports =  {
    'srcDir' : './src',
    'srcFile' : '',
    'ignoreFiles' : [ 'NoMatch.css'],
    'spaceTagClassNameCheck' : ['body', 'div', 'span', 'a'],
    'containTagClassNameCheck' : ['body', 'div' , 'span']

}


```

<a name="run"></a>

## run(configObject)
A exported main function.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| configObject |  | Information about the configFile. |
| configObject.srcDir | <code>string</code> | The source directory for sccs. default value "." |
| configObject.srcFile | <code>string</code> | The single scss file. if provided only single file will be scaned. |
| configObject.ignoreFiles | <code>Array.&lt;string&gt;</code> | The ignoreFiles. default value [] |
| configObject.spaceTagClassNameCheck | <code>Array.&lt;string&gt;</code> | Array of html tag ClassName to chek in scss. default value ['body', 'div', 'span','a'] |
| configObject.containTagClassNameCheck | <code>Array.&lt;string&gt;</code> | Array of contain html tag ClassName to chek in scss. default value [] |



## Rules
1. root classname should be "filename"-component 
2. there should be only one root class in scss file
3. spaceTagClassNameCheck i.e this html tag selectors are not allowed.
    1. not allowed example :
    ```css
    .card {
        body {
            color : red;
        }
    }
    ```
    
    2. not allowed example :
    ```css
    .drop-down {
        div {
            color : blue;
        }
    }
    ```
4. containTagClassNameCheck same as spaceTagClassNameCheck but containg keyword classname not allowed 

## Tests

```sh
npm install
npm test
```

## Dependencies

- [fs](https://ghub.io/fs): 
- [glob](https://ghub.io/glob): a little globber
- [path](https://ghub.io/path): Node.JS path module
- [readline](https://ghub.io/readline): Simple streaming readline module.

## Dev Dependencies

- [jsdoc-to-markdown](https://ghub.io/jsdoc-to-markdown): Generates markdown API documentation from jsdoc annotated source code
- [np](https://ghub.io/np): A better `npm publish`
- [package-json-to-readme](https://ghub.io/package-json-to-readme): Generate a README.md from package.json contents

## License

MIT
